import operaciones
from sys import argv

if __name__ == "__main__":
    if len(argv)>=4:
        try:
            i =1
            while i <= len(argv) - 1:
                operaciones.sumar(int(argv[i]), int(argv[i + 1]))
                operaciones.restar(int(argv[i + 2]), int(argv[i + 3]))
                i += 4
        except IndexError:
            print()
    else:
        operaciones.sumar(1,2)
        operaciones.sumar(3,4)
        operaciones.restar(5,6)
        operaciones.restar(7,8)

